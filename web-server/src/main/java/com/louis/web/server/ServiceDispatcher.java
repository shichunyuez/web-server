package com.louis.web.server;

/**
 * 根据请求类型和URI找到对应的控制器，将请求交给控制器处理
 * 
 * @author Louis
 */
public class ServiceDispatcher {

	/**
	 * 转发处理请求
	 * @param request
	 * @param response
	 */
	public void dispatcher(Request request, Response response) {
		execController(request, response);
	}

	/**
	 * 根据请求类型及URI等请求信息，找到并执行对应的控制器方法后返回
	 * 此处直接返回一个控制器，模拟查找和执行控制器方法的过程
	 * @param request
	 * @param response
	 * @return
	 */
	private void execController(Request request, Response response) {
		String text = getControllerResult(request, response);
		StringBuilder sb = new StringBuilder();
		sb.append("请求类型： " + request.getType());
		sb.append("<br/>请求URI： " + request.getUri());
		sb.append("<br/>返回结果： " + text);
		// 输出控制器返回结果
		response.writeText(sb.toString());
	}

	/**
	 * 模拟查找和执行控制器方法并返回结果
	 * @param request
	 * @param response
	 * @return
	 */
	private String getControllerResult(Request request, Response response) {
		String text = "";
		String uri = request.getUri();
		String [] uriArray = uri.split("\\/");
		if(uriArray.length != 3) {
			text = "请求路径没有找到相关匹配服务. ";
		} else if("test".equalsIgnoreCase(uriArray[1])) {
			TestController testController = new TestController();
			if("test1".equalsIgnoreCase(uriArray[2])) {
				text = testController.test1();
			} else if("test2".equalsIgnoreCase(uriArray[2])) {
				text = testController.test2();
			} else {
				text = "请求路径没有找到相关匹配服务. ";
			}
		} else {
			text = "请求路径没有找到相关匹配服务. ";
		}
		return text;
	}

}
